﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Loggin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Conexion.Loggin(TextBox1.Text, TextBox2.Text, tipoList.Text))
            {
                if (tipoList.Text.Equals("1"))
                    Response.Redirect("ModuloAdmin");
                else if (tipoList.Text.Equals("2")) 
                    Response.Redirect("ModuloAgente");
                else
                    Response.Redirect("ModuloTecnico");


            }
            else
            {
                Response.Write("<script> alert('Usuario no encontrado') </script>");
            }
        }
    }
}
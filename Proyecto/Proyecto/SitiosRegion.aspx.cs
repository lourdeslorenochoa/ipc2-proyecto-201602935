﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class SitiosRegion : System.Web.UI.Page
    {
        public List<Sitio> sitios = new List<Sitio>();
        protected void Page_Load(object sender, EventArgs e)
        {
            sitios = Conexion.connsultaRegion(Request.QueryString["codeGenero"]);
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class ModuloAgente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnInscribir_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile1 = Request.Files["FileUpload1"];

            if (postedFile1 != null && postedFile1.ContentLength > 0)
            {
                string file = Server.MapPath("~/Fotografias/" + Path.GetFileName(postedFile1.FileName));
                postedFile1.SaveAs(file);
                Response.Write("<script> alert('Guardado') </script>");
            }
            Conexion.insertarSitio(TextNombre.Text, TextDescripcion.Text, postedFile1.FileName, tipoList.Text);
        }
    }
}
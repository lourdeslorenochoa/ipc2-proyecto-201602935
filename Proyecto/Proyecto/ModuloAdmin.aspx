﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModuloAdmin.aspx.cs" Inherits="Proyecto.ModuloAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <title></title>
</head>
<body>
     <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="Inicio">Inicio <span class="sr-only">(current)</span></a>
              </li>
                <li class="nav-item active">
                <a class="nav-link" href="ListaDeUsuarios">Empresas<span class="sr-only">(current)</span></a>
              </li>
                
       
            </ul>
          </div>
</nav>
     <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2"></div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">

                <form id="form1" runat="server">
                    <div class ="container">
                        <div class="form-group row">
                            <div>
                            <label for="staticEmail" class="col-sm-2 col-form-label">Sitios turísticos</label>
                            <asp:DropDownList ID ="ListRegion"
                            AutoPostBack="false"
                            runat ="Server" Visible="true" CssClass ="dropdown-trigger btn">
                            <asp:ListItem  Value="1">Norte</asp:ListItem>
                            <asp:ListItem  Value="2">Sur</asp:ListItem>
                            <asp:ListItem  Value="3">Este</asp:ListItem>
                                <asp:ListItem  Value="4">Oeste</asp:ListItem>
                        </asp:DropDownList>
                                <asp:Button ID="BtnRegion" runat="server"  class="btn btn-primary" Text="Buscar" OnClick="BtnRegion_Click"  />

                        </div>
                            <label for="staticEmail" class="col-sm-2 col-form-label">DPI</label>
                            <div class="col-sm-10">
                               <asp:TextBox ID="TextDpi"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                            </div>
                        </div>

                       <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Nombre</label>
                            <div class="col-sm-10">
                                 <asp:TextBox ID="TextNombre"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                            </div>
                       </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                               <asp:TextBox ID="TextEmail"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Teléfono</label>
                            <div class="col-sm-10">
                               <asp:TextBox ID="TextTelefono"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Usuario</label>
                            <div class="col-sm-10">
                               <asp:TextBox ID="TextUsuario"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Contraseña</label>
                            <div class="col-sm-10">
                               <asp:TextBox ID="TextPass"  type="password" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <label for="staticEmail" class="col-sm-2 col-form-label">Tipo de Usuario</label>
                            <asp:DropDownList ID ="tipoList"
                            AutoPostBack="false"
                            runat ="Server" Visible="true" CssClass ="dropdown-trigger btn">
                            <asp:ListItem  Value="1">Administrador</asp:ListItem>
                            <asp:ListItem  Value="2">Agente</asp:ListItem>
                            <asp:ListItem  Value="3">Técnico</asp:ListItem>
                        </asp:DropDownList>

                        </div>
            
                         <asp:Button ID="BtnInsertar" runat="server"  class="btn btn-primary" Text="Insertar" OnClick="Button1_Click"  />
                        <asp:Button ID="BtnModificar" runat="server"  class="btn btn-primary" Text="Modificar" OnClick="BtnModificar_Click" />
                        <asp:Button ID="BtnEliminar" runat="server"  class="btn btn-primary" Text="Eliminar" OnClick="BtnEliminar_Click"  />
                   </div>
                </form>
            </div>
            </div>
         </div>
</body>
</html>

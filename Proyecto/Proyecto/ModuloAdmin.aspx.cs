﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class ModuloAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //int mas grande
            Conexion.insertarUsuario(int.Parse(TextDpi.Text), TextNombre.Text, TextEmail.Text, TextTelefono.Text, tipoList.Text, TextUsuario.Text, TextPass.Text);
            TextDpi.Text = "";
            TextNombre.Text ="";
            TextEmail.Text = "";
            TextTelefono.Text = "";
            TextPass.Text = "";
            TextUsuario.Text = "";
        }

        protected void BtnModificar_Click(object sender, EventArgs e)
        {

        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {

        }

        protected void BtnRegion_Click(object sender, EventArgs e)
        {
            Response.Redirect("SitiosRegion.aspx?codeGenero=" + ListRegion.Text);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SitiosRegion.aspx.cs" Inherits="Proyecto.SitiosRegion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Región</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <% for(int i= 0; i<this.sitios.Count; i++){  %>
                    <tr>
                        <td><%= this.sitios[i].nombre %> </td>
                        <td><%= this.sitios[i].descripcion %> </td>
                        <td><%= this.sitios[i].region %> </td>
                        
                    </tr>
                    <%} %>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto
{
    public class Sitio
    {
        public string region;
        public string nombre;
        public string descripcion;

        public Sitio()
        {
        }

        public Sitio(string region, string nombre, string descripcion)
        {
            Region = region;
            Nombre = nombre;
            Descripcion = descripcion;
            
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Region { get => region; set => region = value; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using MySql.Data;
using Microsoft.SqlServer.Server;




namespace Proyecto
{
    public class Conexion
    {
        public static object Request { get; private set; }


        // private static object Request;

        public static MySqlConnection Conectar()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=1;database=IPC2Proyecto;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            databaseConnection.Open();
            return databaseConnection;
        }
        public static Boolean Loggin(String usuario, String pass, String rol)
        {

            MySqlConnection databaseConnection = Conectar();
            string query = "select  * from   usuario where usuario='" + usuario + "' and contrasena='" + pass +"'and rol = '"+ rol +"';";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    Console.WriteLine(reader.GetString(0));
                    return true;
                }
                Console.WriteLine("No");
                return false;
            }
            catch
            {

            }
            Console.WriteLine("fin");
            return true;
        }
        public static Boolean listaDeUsuarios(string usuario, string pass, string rol)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "SELECT * FROM usuario   WHERE rol = " + rol + ",usuario = " + usuario + ", contrasena =" + pass + ";";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();
                reader = commandDatabase.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {

            }
            return true;
        }
        public static void insertarUsuario(int dpi, string nombre, string email, string tel, string rol, string usuario, string pass)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "insert into usuario (dpi, nombre, email, telefono, rol, usuario, contrasena ) values('" + dpi + "' ,'" + nombre + "','" + email + "','" + tel + "', '" + rol + "','" + usuario+ "', '" + pass+ "');";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos
                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();
                // Hasta el momento todo bien, es decir datos obtenidos
                // IMPORTANTE 
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };

                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }


        }
        
        public static void insertarEmpresa(string nombre, string direccion, string tel, string email, string codE, string foto)
        {
            MySqlConnection databaseConnection = Conectar();
            int correl = 004;
            int idEmpresa = 4;
            string query = "INSERT INTO empresa (idEmpresa, Nombre, Direccion, Telefono, Correo, Estado, Tecnico_DPI, Recibo_Correlativo, CodEmpresa, fotografia) VALUES ('"+idEmpresa+"','" + nombre + "' ,'" + direccion + "','" + tel + "','" + email + "',' prevaluacion ','154','" +correl+ "','"+codE+"','" +foto+ "');";
            correl++;
            idEmpresa++;
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos
                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();
                // Hasta el momento todo bien, es decir datos obtenidos
                // IMPORTANTE 
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };

                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }


        }
        public static void insertarSitio(string nombre, string desc, string foto, string region)
        {
            MySqlConnection databaseConnection = Conectar();
            int correl = 004;
            int idsitio = 0;
            string query = "INSERT INTO sitioturistico (idsitioturistico, Nombre, descripcion, fotografia, idregion) VALUES ('" + idsitio + "','" + nombre + "' ,'" + desc + "','" + foto +"','" + region+"');";
            correl++;
            idsitio++;
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos
                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();
                // Hasta el momento todo bien, es decir datos obtenidos
                // IMPORTANTE 
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };

                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }

        }
        public static List<Sitio> connsultaRegion(string valor)
        {
            List<Sitio> sitios = new List<Sitio>();
            MySqlConnection databaseConnection = Conectar();
            string query = "select sitioturistico.Nombre, sitioturistico.Descripcion, region.nombre from sitioturistico inner join region on sitioturistico.idRegion = region.idRegion where region.idRegion ="+valor+ "; ";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader sdr;
            sdr = commandDatabase.ExecuteReader();

            while (sdr.Read())
            {
                Sitio st = new Sitio();
                st.nombre = sdr["Nombre"].ToString();
                st.descripcion = sdr["Descripcion"].ToString();
                st.region = sdr["nombre"].ToString();
                sitios.Add(st);
            }
            return sitios;

        }
        public static List<Sitio> consultaNombre()
        {
            List<Sitio> sitios = new List<Sitio>();
            MySqlConnection databaseConnection = Conectar();
            string query = "";
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader sdr;
            sdr = commandDatabase.ExecuteReader();
            while (sdr.Read())
            {

            }
            return sitios;
        }
    }
}
CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`usuario` (
  `DPI` INT(11) NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NULL DEFAULT NULL,
  `Telefono` VARCHAR(12) NULL DEFAULT NULL,
  `Rol` VARCHAR(45) NOT NULL,
  `usuario` VARCHAR(45) NOT NULL,
  `contrasena` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`DPI`));

CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`recibo` (
  `Correlativo` INT(11) NOT NULL,
  PRIMARY KEY (`Correlativo`));

  CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`Detalle` (
  `id` INT NOT NULL,
  `Horario` VARCHAR(45) NOT NULL,
  `Tarifa` INT NOT NULL,
  
;
CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`servicios` (
  `CodServicio` INT(11) NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`CodServicio`))
;


CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`region` (
  `idRegion` INT(11) NOT NULL,
  `Nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idRegion`))
;
CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`regionempresa` (
  `idRegionEmpresa` INT(11) NOT NULL,
  `Region_idRegion` INT(11) NOT NULL,
  `Empresa_idEmpresa` INT(11) NOT NULL,
  PRIMARY KEY (`idRegionEmpresa`),
  INDEX `Region_idRegion` (`Region_idRegion` ASC) VISIBLE,
  INDEX `Empresa_idEmpresa` (`Empresa_idEmpresa` ASC) VISIBLE,
  CONSTRAINT `regionempresa_ibfk_1`
    FOREIGN KEY (`Region_idRegion`)
    REFERENCES `ipc2proyecto`.`region` (`idRegion`),
  CONSTRAINT `regionempresa_ibfk_2`
    FOREIGN KEY (`Empresa_idEmpresa`)
    REFERENCES `ipc2proyecto`.`empresa` (`idEmpresa`))
;



create table sitioTuristico(
   idSitioturistico INT NOT NULL,
   Nombre VARCHAR(45) NOT NULL,
   Descripcion VARCHAR(45) NOT NULL,
   Fotografia BLOB NOT NULL,
   idRegion int NOT NULL,
   primary key (idSitioturistico),
   index(idSitioturistico),
   foreign key (idRegion) references region (idRegion)
);

CREATE TABLE Empresa (
  idEmpresa INT NOT NULL,
  Nombre VARCHAR(45) NOT NULL,
  Direccion VARCHAR(45) NOT NULL,
  Telefono VARCHAR(45) NOT NULL,
  Correo VARCHAR(45) NULL,
  Estado VARCHAR(45) NOT NULL,
  Tecnico_DPI INT NOT NULL,
  Recibo_Correlativo INT NOT NULL,
  CodEmpresa INT NOT NULL,
  PRIMARY KEY (idEmpresa),
  INDEX  (Tecnico_DPI),
  INDEX  (Recibo_Correlativo),
  FOREIGN KEY (Tecnico_DPI) REFERENCES Usuario (DPI),
  FOREIGN KEY (Recibo_Correlativo) REFERENCES Recibo (Correlativo)
);  
CREATE TABLE RegionEmpresa (
  idRegionEmpresa INT NOT NULL,
  Region_idRegion INT NOT NULL,
  Empresa_idEmpresa INT NOT NULL,
  PRIMARY KEY (idRegionEmpresa),
  INDEX (Region_idRegion),
  INDEX  (Empresa_idEmpresa) ,
  FOREIGN KEY (Region_idRegion)REFERENCES Region (idRegion),
  FOREIGN KEY (Empresa_idEmpresa) REFERENCES Empresa (idEmpresa)
);
CREATE TABLE IF NOT EXISTS empresaservicio (
  Servicios_CodServicio INT NOT NULL,
  Empresa_idEmpresa INT NOT NULL,
  INDEX (Servicios_CodServicio) ,
  INDEX (Empresa_idEmpresa),
  PRIMARY KEY (Servicios_CodServicio, Empresa_idEmpresa),
  FOREIGN KEY (Servicios_CodServicio) REFERENCES Servicios (CodServicio),
  FOREIGN KEY (Empresa_idEmpresa) REFERENCES Empresa (idEmpresa)
);
   
CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`Especialidad` (
  `codEspecialidad` INT NOT NULL AUTO_INCREMENT,
  `Descripcion` VARCHAR(45) NOT NULL,
  `empresa_idEmpresa` INT(11) NOT NULL,
  PRIMARY KEY (`codEspecialidad`),
  INDEX `fk_Especialidad_empresa1_idx` (`empresa_idEmpresa` ASC) VISIBLE,
  CONSTRAINT `fk_Especialidad_empresa1`
    FOREIGN KEY (`empresa_idEmpresa`)
    REFERENCES `ipc2proyecto`.`empresa` (`idEmpresa`)
    ;

CREATE TABLE IF NOT EXISTS `ipc2proyecto`.`DetalleEspecialidad` (
  `empresa_idEmpresa` INT(11) NOT NULL,
  INDEX `fk_DetalleEspecialidad_empresa1_idx` (`empresa_idEmpresa` ASC) VISIBLE,
  CONSTRAINT `fk_DetalleEspecialidad_empresa1`
    FOREIGN KEY (`empresa_idEmpresa`)
    REFERENCES `ipc2proyecto`.`empresa` (`idEmpresa`)
);
